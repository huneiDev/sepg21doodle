﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraFollow : MonoBehaviour {

    [SerializeField]
    private GameObject doodle;
    private int score;
    [SerializeField]
    private Text scoreText;

    [SerializeField]
    private Text highscoreText;

    [SerializeField]
    private int highscore;

    [SerializeField]
    private Generator generator;

    public int Score {

        get {

            return score;

        }

    }

    public void Save() {
        PlayerPrefs.SetInt("highscore", highscore);
        PlayerPrefs.Save();
    }

    void Start() {

        if (PlayerPrefs.HasKey("highscore")) {
            highscore = PlayerPrefs.GetInt("highscore");
            highscoreText.text = "Highscore: " + highscore;
        }

    }
    void OnApplicationQuit() {

    

    }

	// Update is called once per frame
	void Update () {

        // Jeigu musu doodle pakilo auksciau nei kamera.
        if (transform.position.y < doodle.transform.position.y)
        {
            transform.position = new Vector3(transform.position.x, doodle.transform.position.y, -10);
            score = Mathf.CeilToInt(transform.position.y) * 100;
            Debug.Log("Bandau gautio" + generator.currentDifficulty * generator.difficultyMultipier);
            generator.currentDifficulty += generator.difficultyMultipier / 100f;
            if(score > highscore) {
                highscore = score;
                highscoreText.text = "Highscore: " + highscore;
            }
            scoreText.text = score.ToString();
        }
	
	}
}
