﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Generator : MonoBehaviour  {

    private float kitasY;
    [SerializeField]
    private CameraFollow cameraFollow;

    [SerializeField]
    private GameObject greenPlat;
  
    public float currentDifficulty;

    public float difficultyMultipier;

    [SerializeField]
    private List<GameObject> allPlatforms;

    [SerializeField]
    private int maxPlatforms;

    [SerializeField]
    private int previousPlat;
     


 
    void Update() {

        
    }

   public void SukurtiPlatforma(int amount){


        for (int i = 0; i < amount; i++) {

            if (currentDifficulty < 0.5f) {
                maxPlatforms = 1;        
            }
            if (currentDifficulty > 0.5f) {
                maxPlatforms = 2;
            }
            if (currentDifficulty > 1) {

                maxPlatforms = 3;
            } if(currentDifficulty > 2) {
                maxPlatforms = 4;

            } if(currentDifficulty > 3) {
                maxPlatforms = 5;
            }
            int randomItem = Random.Range(0, maxPlatforms);
            if (randomItem != 0) {
                if (maxPlatforms > 1) {
                    while (randomItem == previousPlat) {
                        randomItem = Random.Range(0, maxPlatforms);
                    }

                }
            }
            previousPlat = randomItem;   
            // previous = raudonui
            float randomX = Random.Range(-2.3f, 2.3f);
            GameObject newPlatform = (GameObject)Instantiate(allPlatforms[randomItem]);
            newPlatform.transform.position = new Vector2(randomX, kitasY);
            kitasY += 1.8f;
        }
    }

    public void Retry(){

        Time.timeScale = 1;
        Application.LoadLevel(0);

    }

    void Start(){

      
        for (int i = 0; i < 20; i++) {

            SukurtiPlatforma(1);

        }

    }
}
