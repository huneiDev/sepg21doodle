﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {


	public float speed;
	public Sprite brokenPlat;
    public Sprite spring;

    [SerializeField]
    private Generator generator;

    [SerializeField]
    private CameraFollow cameraFollow;

    [SerializeField]
    private GameObject losePanel;
	// Use this for initialization
	void Start () {
	
	}

	
	// Update is called once per frame
	void Update () {




        if (transform.position.x > 2.9f)
        {
            transform.position = new Vector2(-2.9f, transform.position.y);

        }
        else if (transform.position.x < -2.9f)
        {
            transform.position = new Vector2(2.9f, transform.position.y);

        }

		if (GetComponent<Rigidbody2D> ().velocity.y > 0)
		{
			GetComponent<BoxCollider2D> ().enabled = false;

		}
		else
		{
			GetComponent<BoxCollider2D> ().enabled = true;

		}
	
		if (Input.GetKey (KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
		{
			
			transform.Translate (Vector3.left * Time.deltaTime * speed);
            transform.localScale = new Vector2(-1, 1);
        
		}

		if (Input.GetKey (KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
		{

			transform.Translate (Vector3.right * Time.deltaTime * speed);
            transform.localScale = new Vector2(1, 1);
		}
	}

    void OnTriggerEnter2D(Collider2D atsitrenkimas) {

        if(atsitrenkimas.gameObject.tag == "MainCamera") {
            cameraFollow.Save();
            Time.timeScale = 0;
            losePanel.SetActive(true);
          
        }

    }

	void OnCollisionEnter2D(Collision2D atsitrenkimas)
	{

        generator.SukurtiPlatforma(1);
        
        if (atsitrenkimas.gameObject.tag == "Spring")
        {
            atsitrenkimas.gameObject.GetComponent<SpriteRenderer>().sprite = spring;
            GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, 800 ));
            generator.SukurtiPlatforma(10);

        }
    

		if (atsitrenkimas.gameObject.tag == "GreenPlat")
		{

			GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, 400));

		}


		if (atsitrenkimas.gameObject.tag == "RedPlat")
		{
			atsitrenkimas.gameObject.GetComponent<SpriteRenderer>().sprite = brokenPlat;
			atsitrenkimas.gameObject.GetComponent<Collider2D>().enabled = false;

		}


		if (atsitrenkimas.gameObject.tag == "WhitePlat")
		{
            GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, 400));
			Destroy(atsitrenkimas.gameObject);

		}




	}

}
