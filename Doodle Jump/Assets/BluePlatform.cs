﻿using UnityEngine;
using System.Collections;

public class BluePlatform : MonoBehaviour {

    // Pradine kryptis.
    Vector2 direction = Vector2.left;

    // Platformos judejimo greitis
    public float speed;

    void Update()
    {
        //Jeigu mes pasiekem kairiaji krasta.
        if (transform.position.x < -2.2f)
        {
            direction = Vector2.right;

        }
        //Jeigu mes pasiekem desianyji krasta.
        if (transform.position.x > 2.2f)
        {
            direction = Vector2.left;

        }

        transform.Translate(direction * speed);



    }


}
